package sandbox.typeclasses

/**
  * Created by erusak@exadel.com on 4/28/18.
  */
trait Printable[A] {
  def format(value: A): String
}

object Printable {

  def format[A](value: A)(implicit printable: Printable[A]): String = printable.format(value)

  def print[A](value: A)(implicit printable: Printable[A]): Unit = println(printable.format(value))

}

object PrintableInstances {

  implicit final object PrintableString extends Printable[String] {
    override def format(value: String): String = value
  }

  implicit final object PrintableInt extends Printable[Int] {
    override def format(value: Int): String = value.toString
  }
}

object PrintableSyntax {
  implicit class PrintableOps[A](val value: A) extends AnyVal {
    def format(implicit printable: Printable[A]): String = printable.format(value)
    def print(implicit printable: Printable[A]): Unit = println(printable.format(value))
  }
}