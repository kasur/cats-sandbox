package sandbox.typeclasses

/**
  * Created by erusak@exadel.com on 4/28/18.
  */
final case class Cat(name: String, age: Int, color: String)

object Cat {
  implicit final object PrintableCat extends Printable[Cat] {
    override def format(value: Cat): String = {
      val Cat(name, age, color) = value
      s"$name is a $age year-old $color cat"
    }
  }
}
