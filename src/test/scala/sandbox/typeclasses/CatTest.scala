package sandbox.typeclasses

import org.scalatest.{FlatSpec, MustMatchers}

/**
  * Created by erusak@exadel.com on 4/28/18.
  */
class CatTest extends FlatSpec with MustMatchers {

  val cat: Cat = Cat("Pluto", 7, "black")

  val expectedFormat: String = "Pluto is a 7 year-old black cat"

  it should "be possible to use homemade type classes" in {

    Printable.format(cat) mustBe expectedFormat

  }

  it should "also work for Printable Syntax" in {
    import PrintableSyntax._

    cat.format mustBe expectedFormat

  }
}
